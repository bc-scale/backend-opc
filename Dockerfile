FROM node:18-alpine3.15
WORKDIR /app
COPY package.json .
COPY development.env .
RUN npm install -g npm@latest
RUN npm i
RUN npm i -g nodemon
RUN npm i -g cross-env
COPY . .
EXPOSE 3030
CMD ["npm", "run", "dev"]
