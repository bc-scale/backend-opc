"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordInvalid = void 0;
const _1 = require(".");
class PasswordInvalid extends _1.Exception {
    constructor(password) {
        super(`The password ${password} is invalid`);
        this.name = 'PasswordInvalid';
        this.spanishMessage = 'La contraseña no es válida';
        this.message = `The password ${password} is invalid`;
    }
}
exports.PasswordInvalid = PasswordInvalid;
