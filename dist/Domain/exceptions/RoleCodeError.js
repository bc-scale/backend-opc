"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCodeError = void 0;
const _1 = require(".");
class RoleCodeError extends _1.Exception {
    constructor(message) {
        super(message);
        this.name = 'RoleCodeError';
        this.spanishMessage = 'El código de rol no es válido';
        this.message = message !== null && message !== void 0 ? message : this.spanishMessage;
    }
}
exports.RoleCodeError = RoleCodeError;
