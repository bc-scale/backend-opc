"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffAdministrative = void 0;
const Staff_1 = require("./Staff");
class StaffAdministrative extends Staff_1.Staff {
    constructor(props) {
        super(props);
    }
}
exports.StaffAdministrative = StaffAdministrative;
