"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.roleCode = void 0;
var roleCode;
(function (roleCode) {
    roleCode["SUPERADMIN"] = "SUPERADMIN";
    roleCode["OPCCoordinator"] = "opc-coordinator";
    roleCode["OPCAssistant"] = "opc-assistant";
    roleCode["ADMINTeacher"] = "admin-professor";
    roleCode["ADMINExternal"] = "admin-external";
})(roleCode = exports.roleCode || (exports.roleCode = {}));
