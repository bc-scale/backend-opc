"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EmailInvalid_1 = require("@domain/exceptions/EmailInvalid");
const passwordInvalid_1 = require("@domain/exceptions/passwordInvalid");
const RoleCodeError_1 = require("@domain/exceptions/RoleCodeError");
const StaffAlreadyExist_1 = require("@domain/exceptions/StaffAlreadyExist");
const StaffValidations_1 = require("@domain/services/StaffValidations");
describe('StaffValidations', () => {
    const staffRepository = {
        save: jest.fn(),
        edit: jest.fn(),
        get: jest.fn(),
        getByEmail: jest.fn()
    };
    const staff = {
        dni: '12345678A',
        name: 'John',
        surname: 'Doe',
        email: 'jhon',
        password: '12345678A',
        roleCode: 'OPCAssistant'
    };
    const staffValidations = new StaffValidations_1.StaffValidations(staffRepository);
    describe('runValidations', () => {
        it("Should throw an exception if the staff's dni is invalid", async () => {
            staffRepository.get.mockResolvedValueOnce(staff);
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(StaffAlreadyExist_1.StaffAlreadyExist);
        });
        it("Should throw an exception if the staff's email is invalid", async () => {
            staffRepository.getByEmail.mockResolvedValueOnce(staff);
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(StaffAlreadyExist_1.StaffAlreadyExist);
        });
        it("Should throw an exception if the staff's password is invalid (Password must be at least 8 characters long)", async () => {
            staff.password = '1234567';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(passwordInvalid_1.PasswordInvalid);
        });
        it("Should throw an exception if the staff's password is invalid (Password must contain at least one uppercase letter)", async () => {
            staff.password = '12345678';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(passwordInvalid_1.PasswordInvalid);
        });
        it("Should throw an exception if the staff's password is invalid (Password must contain at least one lowercase letter)", async () => {
            staff.password = '12345678A';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(passwordInvalid_1.PasswordInvalid);
        });
        it("Should throw an exception if the staff's password is invalid (Password must contain at least one number)", async () => {
            staff.password = 'abcdefghdasdASADS';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(passwordInvalid_1.PasswordInvalid);
        });
        it("Should throw an exception if the staff's email format is invalid", async () => {
            staff.password = '12345678aA';
            staff.email = 'jhon';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(EmailInvalid_1.EmailInvalid);
        });
        it("Should throw an exception if the staff's role is invalid", async () => {
            staff.roleCode = 'Student';
            staff.email = 'jhondue@hotmail.com';
            await expect(staffValidations.runValidations(staff)).rejects.toThrowError(RoleCodeError_1.RoleCodeError);
        });
        it('Should return void if the staff is valid', async () => {
            staff.roleCode = 'OPCAssistant';
            await expect(staffValidations.runValidations(staff)).resolves.toBeUndefined();
        });
    });
});
