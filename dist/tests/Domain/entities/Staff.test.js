"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Staff_1 = require("@domain/entities/Staff");
const StaffAdministrative_1 = require("@domain/entities/Staff/StaffAdministrative");
const StaffOPC_1 = require("@domain/entities/Staff/StaffOPC");
describe('Staff', () => {
    it('should be able to create a new Staff with OPCAssistant role', () => {
        const staff = (0, Staff_1.getStaffInstance)({
            dni: '12345678',
            name: 'John',
            email: 'jhon',
            password: '12345678A',
            roleCode: 'OPCAssistant'
        });
        expect(staff).toBeInstanceOf(StaffOPC_1.StaffOPC);
    });
    it('should be able to create a new Staff with OPCCoordinator role', () => {
        const staff = (0, Staff_1.getStaffInstance)({
            dni: '12345678',
            name: 'John',
            email: 'jhon',
            password: '12345678A',
            roleCode: 'OPCCoordinator'
        });
        expect(staff).toBeInstanceOf(StaffOPC_1.StaffOPC);
    });
    it('should be able to create a new Staff with ADMINExternal role', () => {
        const staff = (0, Staff_1.getStaffInstance)({
            dni: '12345678',
            name: 'John',
            email: 'jhon',
            password: '12345678A',
            roleCode: 'ADMINExternal'
        });
        expect(staff).toBeInstanceOf(StaffAdministrative_1.StaffAdministrative);
    });
    it('should be able to create a new Staff with ADMINTeacher role', () => {
        const staff = (0, Staff_1.getStaffInstance)({
            dni: '12345678',
            name: 'John',
            email: 'jhon',
            password: '12345678A',
            roleCode: 'ADMINTeacher'
        });
        expect(staff).toBeInstanceOf(StaffAdministrative_1.StaffAdministrative);
    });
    it('should return undefined if the role is invalid', () => {
        const staff = (0, Staff_1.getStaffInstance)({
            dni: '12345678',
            name: 'John',
            email: 'jhon',
            password: '12345678A',
            roleCode: 'Student'
        });
        expect(staff).toBeUndefined();
    });
});
