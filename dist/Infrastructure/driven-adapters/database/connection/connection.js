"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pool = exports.client = void 0;
const config_1 = __importDefault(require("@infrastructure/config/config"));
const logger_1 = require("@infrastructure/driven-adapters/logger");
const pg_1 = require("pg");
const pool = new pg_1.Pool(config_1.default.DATABASE);
exports.pool = pool;
const client = new pg_1.Client(config_1.default.DATABASE_CLIENT);
exports.client = client;
void client.connect();
client.query('SELECT NOW()', (err, res) => {
    const loggerService = new logger_1.LoggerService();
    try {
        loggerService.log(res.rows[0]);
        void client.end();
    }
    catch (error) {
        loggerService.log(err);
    }
});
