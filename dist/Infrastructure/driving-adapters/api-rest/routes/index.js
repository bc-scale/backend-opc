"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const exceptions_1 = require("@domain/exceptions");
const express_1 = require("express");
const route = (0, express_1.Router)();
route.use('/api/v0', function (req, res) {
    res.send('hello world');
});
route.use((err, req, res, next) => {
    if (err instanceof exceptions_1.Exception) {
        res.status(400).json({
            message: err.message
        });
    }
    else {
        next(err);
    }
});
route.use((err, req, res, next) => {
    res.status(500);
    res.json({
        error: err
    });
});
exports.default = route;
