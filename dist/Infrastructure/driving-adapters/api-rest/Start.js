"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("@infrastructure/driven-adapters/database/connection");
const OPCApp_1 = require("./OPCApp");
try {
    connection_1.PostgresConnection.getInstance();
    void new OPCApp_1.OPCApp().start();
}
catch (error) {
    console.log(error);
}
