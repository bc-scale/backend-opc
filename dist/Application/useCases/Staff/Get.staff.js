"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetUserStaffCase = void 0;
const Staff_mapper_1 = require("@application/mappers/Staff.mapper");
const UserNotFound_1 = require("@domain/exceptions/UserNotFound");
class GetUserStaffCase {
    constructor(staffRepository) {
        this.staffRepository = staffRepository;
    }
    async run(dni) {
        const staff = await this.staffRepository.get(dni);
        if (staff === undefined || staff === null)
            throw new UserNotFound_1.UserNotFound('User not found');
        return Staff_mapper_1.StaffMapperDto.domainToDto(staff);
    }
}
exports.GetUserStaffCase = GetUserStaffCase;
