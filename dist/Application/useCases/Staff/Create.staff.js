"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateStaffUseCase = void 0;
const Staff_mapper_1 = require("@application/mappers/Staff.mapper");
const exceptions_1 = require("@domain/exceptions");
const StaffValidations_1 = require("@domain/services/StaffValidations");
class CreateStaffUseCase {
    constructor(staffRepository) {
        this.staffRepository = staffRepository;
    }
    async run(props) {
        const staff = Staff_mapper_1.StaffMapperDto.toDomain(props);
        if (staff === undefined || staff === null)
            throw new exceptions_1.Exception('Error creating user');
        const staffValidations = new StaffValidations_1.StaffValidations(this.staffRepository);
        await staffValidations.runValidations(staff);
        const staffCreated = await this.staffRepository.save(staff);
        return Staff_mapper_1.StaffMapperDto.domainToDto(staffCreated);
    }
}
exports.CreateStaffUseCase = CreateStaffUseCase;
