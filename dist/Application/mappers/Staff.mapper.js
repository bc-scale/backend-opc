"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffMapperDto = void 0;
const Staff_1 = require("@domain/entities/Staff");
exports.StaffMapperDto = {
    domainToDto: (staff) => {
        return {
            dni: staff.dni,
            name: staff.name,
            email: staff.email,
            cellphone: staff.cellphone,
            password: staff.password,
            role: staff.roleCode,
            token: staff.token
        };
    },
    domainListToDtoList: (staffList) => {
        return staffList.map((staff) => exports.StaffMapperDto.domainToDto(staff));
    },
    toDomain: (StaffDto) => {
        const staff = {
            dni: StaffDto.dni,
            name: StaffDto.name,
            email: StaffDto.email,
            cellphone: StaffDto.cellphone,
            password: StaffDto.password,
            roleCode: StaffDto.role,
            token: StaffDto.token
        };
        return (0, Staff_1.getStaffInstance)(staff);
    },
    toDomainList: (StaffDtoList) => {
        return StaffDtoList.users.map((StaffDto) => exports.StaffMapperDto.toDomain(StaffDto));
    }
};
