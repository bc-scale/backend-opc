# Backend-OPC UDEM

## Descripción

Servicio de reservación de espacios para eventos internos de la Universidad de Medellín

## Clonar el repositorio

```bash
git clone git@gitlab.com:code-hub-house/backend-opc.git
```

## Instalar dependencias

```bash
npm install
```

## Ejecutar el proyecto

### Modo desarrollo

```bash
npm run dev
```

### Modo producción

```bash
npm run build
npm run prod
```

## Ejecutar pruebas

```bash
npm run test
```

## Ejecutar pruebas con cobertura

```bash
npm run test:cov
```

# Docker

## Construir imagen

```bash
docker build -t backend-opc .
```

## Construir docker-compose en modo desarrollo

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
```


## Ejecutar docker-compose en modo desarrollo

```bash
docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```

## Ejecutar docker-compose en modo producción

```bash
docker compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```

## Construir docker-compose en modo producción

```bash
docker-compose -f docker-compose.yml -f docker-compose.prod.yml build
```