import { ReservationProps } from '@domain/interfaces/ReservationProps';

export interface ReservationRepository {
  createReservation: (reservation: ReservationProps) => Promise<ReservationProps | null | undefined>
  getAllReservations: () => Promise<ReservationProps[] | null | undefined>
  getReservationById: (id: string) => Promise<ReservationProps | null | undefined>
  getReservationByEventId: (eventId: string) => Promise<ReservationProps[] | null | undefined>
  getReservationBySpaceId: (spaceId: string) => Promise<ReservationProps[] | null | undefined>
  updateReservation: (reservation: ReservationProps) => Promise<ReservationProps | null | undefined>
  deleteReservation: (id: string) => Promise<ReservationProps | null | undefined>
  getReservationByState: (state: string) => Promise<ReservationProps[] | null | undefined>
  setState: (id: string, state: string) => Promise<ReservationProps | null | undefined>
  getReservationsByUser: (userId: string) => Promise<ReservationProps[] | null | undefined>
  addProduct: (id: string, productId: string) => Promise<ReservationProps | null | undefined>
}
