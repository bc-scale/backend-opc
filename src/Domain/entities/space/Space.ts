export interface Space {
  id: string
  name: string
  number: number
  capacity: number
  state: string
}
