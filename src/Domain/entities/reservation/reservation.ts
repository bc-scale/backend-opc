import { ReservationProps } from '@domain/interfaces/ReservationProps';
import { TicketReservation } from './ticket';

export class Reservation implements ReservationProps {
  id: string;
  event_id: string;
  space_id: string;
  ticket: TicketReservation;
  event?: any;
  space?: any;

  constructor (props: ReservationProps) {
    this.id = props.id;
    this.event_id = props.event_id;
    this.space_id = props.space_id;
    this.ticket = props.ticket;
  }
}
