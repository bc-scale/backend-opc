import { State } from '@domain/enums/State';

export interface TicketReservation {
  id: string
  cost_center: string
  overtime: boolean
  overtime_description?: string
  dependence: string
  state: keyof typeof State
}
