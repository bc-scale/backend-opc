import { roleCode } from '@domain/enums/Role';
import { StaffProps } from '@domain/interfaces/StaffProps';

export abstract class Staff implements StaffProps {
  readonly dni: string;
  name: string;
  email: string;
  cellphone: string;
  password: string;
  roleCode: keyof typeof roleCode;
  token?: string;

  constructor (props: StaffProps) {
    this.dni = props.dni;
    this.name = props.name;
    this.email = props.email;
    this.cellphone = props.cellphone;
    this.password = props.password;
    this.roleCode = props.roleCode;
  }

  verifyPassword? (password: string): boolean {
    return this.password === password;
  }
}
