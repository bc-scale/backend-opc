import { roleCode } from '@domain/enums/Role';
import { StaffProps } from '@domain/interfaces/StaffProps';
import { AdminExternal } from './AdminExternal';
import { AdminTeacher } from './AdminTeacher';
import { OPCAssistant } from './OPCAssistant';
import { OPCCoordinator } from './OPCCoordinator';
import { Staff } from './Staff';
import { SuperAdmin } from './SuperAdmin';

export const getStaffInstance = (props: StaffProps): Staff | undefined => {
  switch (roleCode[props.roleCode]) {
    case roleCode['opc-assistant']:
      return new OPCAssistant(props);
    case roleCode['opc-coordinator']:
      return new OPCCoordinator(props);
    case roleCode['admin-professor']:
      return new AdminExternal(props);
    case roleCode['admin-external']:
      return new AdminTeacher(props);
    case roleCode.SUPERADMIN:
      return new SuperAdmin(props);
  }
};
