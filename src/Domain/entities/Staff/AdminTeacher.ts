import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffAdministrative } from './StaffAdministrative';

export class AdminTeacher extends StaffAdministrative {
  constructor (props: StaffProps) {
    super(props);
  }
}
