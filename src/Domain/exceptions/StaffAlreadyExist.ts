import { Exception } from '.';

export class StaffAlreadyExist extends Exception {
  constructor (message: string) {
    super(message);
    this.name = 'StaffAlreadyExist';
    this.spanishMessage = 'El personal ya existe';
  }
}
