import { Exception } from '.';

export class EventNotFound extends Exception {
  constructor (message: string) {
    super(message);
  }
}
