import { TicketReservation } from '@domain/entities/reservation/ticket';

export interface ReservationProps {
  id: string
  event_id: string
  space_id: string
  ticket: TicketReservation
  space?: any
  event?: any
}
