import { roleCode } from '@domain/enums/Role';
import { CellphoneInvalid } from '@domain/exceptions/CellphoneInvalid';
import { EmailInvalid } from '@domain/exceptions/EmailInvalid';
import { PasswordInvalid } from '@domain/exceptions/passwordInvalid';
import { RoleCodeError } from '@domain/exceptions/RoleCodeError';
import { StaffAlreadyExist } from '@domain/exceptions/StaffAlreadyExist';
import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffRepository } from '@domain/repositories/Staff';

export class StaffValidations {
  private readonly staffRepository: StaffRepository;

  constructor (staffRepository: StaffRepository) {
    this.staffRepository = staffRepository;
  }

  async runValidationsToCreate (staff: StaffProps): Promise<void> {
    await this.validateDni(staff.dni);
    await this.validateEmail(staff.email);
    this.validatePassword(staff.password);
    this.validateEmailFormat(staff.email);
    this.validateCellphone(staff.cellphone);
    this.validateRole(roleCode[staff.roleCode]);
  }

  async runValidationsToEdit (staff: StaffProps): Promise<void> {
    this.validatePassword(staff.password);
    this.validateEmailFormat(staff.email);
    this.validateCellphone(staff.cellphone);
    this.validateRole(roleCode[staff.roleCode]);
  }

  async validateDni (dni: string): Promise<void> {
    const staff = await this.staffRepository.get(dni);
    if (staff !== undefined) {
      throw new StaffAlreadyExist('Staff with this DNI already exists');
    }
  }

  async validateEmail (email: string): Promise<void> {
    const staff = await this.staffRepository.getByEmail(email);
    if (staff !== undefined) {
      throw new StaffAlreadyExist('Staff with this email already exists');
    }
  }

  validatePassword (password: string): void {
    if (password.length < 8) {
      throw new PasswordInvalid('Password must be at least 8 characters long');
    }
    if (!/[A-Z]/.test(password)) {
      throw new PasswordInvalid('Password must contain at least one uppercase letter');
    }
    if (!/[a-z]/.test(password)) {
      throw new PasswordInvalid('Password must contain at least one lowercase letter');
    }
    if (!/[0-9]/.test(password)) {
      throw new PasswordInvalid('Password must contain at least one number');
    }
  }

  validateEmailFormat (email: string): void {
    // eslint-disable-next-line no-useless-escape
    const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (!regex.test(email)) {
      throw new EmailInvalid('Email format is not valid');
    }
  }

  validateRole (rc: roleCode): void {
    const roles = Object.values(roleCode);
    if (!roles.includes(rc)) {
      throw new RoleCodeError('Role code is not valid');
    }
  }

  validateCellphone (cellphone: string): void {
    const regex = /^3[0-9]{9}$/;
    if (!regex.test(cellphone)) {
      throw new CellphoneInvalid('Cellphone is not valid');
    }
  }
}
