import { OPCApp } from '@infrastructure/driving-adapters/api-rest/OPCApp';

describe('Server', () => {
  const server = new OPCApp();

  it("The port should be '4040'", async () => {
    await server.start();
    expect(server.port).toBe('4040');
    await server.stop();
  });

  it('should start the server', async () => {
    await server.start().then(() => {
      expect(server).toBeDefined();
    });
    await server.stop();
  });

  it('should stop the server', async () => {
    await server.stop().then(() => {
      expect(server).toBeDefined();
    });
  });

  it('should throw an error if the server is already started', async () => {
    await server.start().catch((error) => {
      expect(error).toBeDefined();
    });
    await server.stop();
  }, 10000);

  it('should throw an error if the server is already stopped', async () => {
    await server.stop().catch((error) => {
      expect(error).toBeDefined();
    });
  });
});
