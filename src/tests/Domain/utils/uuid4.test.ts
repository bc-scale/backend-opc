import { uuidv4 } from '@domain/utils/uuidv4';

describe('uuidv4', () => {
  it('should return a valid uuid v4', () => {
    const uuid = uuidv4();
    expect(uuid).toMatch(
      /[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}/
    );
  });

  it('should return a different uuid each time', () => {
    const listUuid = [];
    for (let i = 0; i < 100; i++) {
      const uuid = uuidv4();
      expect(listUuid).not.toContain(uuid);
      listUuid.push(uuid);
    }
  });
});
