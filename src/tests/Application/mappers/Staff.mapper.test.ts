import { StaffDto } from '@application/dto/Staff.dto';
import { StaffMapperDto } from '@application/mappers/Staff.mapper';
import { StaffProps } from '@domain/interfaces/StaffProps';

describe('StaffMapperDto', () => {
  const staffDomain: StaffProps = {
    dni: '12345678',
    name: 'John Doe',
    email: 'jhonDown@gmail.com',
    cellphone: '3001234567',
    password: '12345678Aa',
    roleCode: 'ADMINTeacher'
  };

  const staffDto: StaffDto = {
    dni: '12345678',
    name: 'John Doe',
    email: 'jhonDown@gmail.com',
    cellphone: '3001234567',
    password: '12345678Aa',
    role: 'ADMINTeacher'
  };

  it('should be defined', () => {
    expect(StaffMapperDto).toBeDefined();
  });

  it('should be able to map a Staff domain to a StaffDto', () => {
    const staffDto: StaffDto = StaffMapperDto.domainToDto(staffDomain);
    expect(staffDto).toEqual(staffDto);
  });

  it('should be able to map a StaffDto to a Staff domain', () => {
    const staffDomain = StaffMapperDto.toDomain(staffDto);
    expect(staffDomain).toEqual(staffDomain);
  });

  it('should be able to map a StaffDtoList to a Staff domain list', () => {
    const staffDtoList = {
      users: [staffDto, staffDto]
    };
    const staffDomainList = StaffMapperDto.toDomainList(staffDtoList);
    expect(staffDomainList).toEqual([staffDomain, staffDomain]);
  });

  it('should be able to map a Staff domain list to a StaffDtoList', () => {
    const staffDtoList = StaffMapperDto.domainListToDtoList([staffDomain, staffDomain]);
    expect(staffDtoList).toEqual([staffDto, staffDto]);
  });
});
