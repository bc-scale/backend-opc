import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class GetReservationByStateUseCase {
  constructor (private readonly repository: ReservationRepository) {}

  async run (state: string): Promise<ReservationDto[]> {
    const reservations = await this.repository.getReservationByState(state);
    if (reservations === null || reservations === undefined) {
      throw new Error('Reservation not found');
    }
    const reservationsDTO = ReservationMapper.toDtoList(reservations);
    return reservationsDTO;
  }
}
