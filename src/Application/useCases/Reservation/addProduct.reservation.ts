import { ReservationRepository } from '@domain/repositories/Reservation';

export class AddProductToReservationUseCase {
  constructor (
    private readonly reservationRepository: ReservationRepository
  ) {}

  async run (id: string, productId: string): Promise<void> {
    await this.reservationRepository.addProduct(id, productId);
  }
}
