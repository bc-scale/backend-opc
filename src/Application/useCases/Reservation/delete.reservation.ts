import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class DeleteReservationUseCase {
  constructor (
    private readonly reservationRepository: ReservationRepository
  ) {}

  async run (id: string): Promise<ReservationDto> {
    const reservation = await this.reservationRepository.deleteReservation(id);
    if (reservation === undefined || reservation === null) throw new Error('Error deleting reservation');
    const reservationDto = ReservationMapper.toDto(reservation);
    return reservationDto;
  }
}
