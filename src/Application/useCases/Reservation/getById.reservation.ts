import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { Exception } from '@domain/exceptions';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class GetByIdReservationUseCase {
  constructor (
    private readonly repository: ReservationRepository
  ) {}

  async run (id: string): Promise<ReservationDto> {
    const reservation = await this.repository.getReservationById(id);
    if (reservation === undefined || reservation === null) throw new Exception('Reservation not found');
    const reservationDto = ReservationMapper.toDto(reservation);
    return reservationDto;
  }
}
