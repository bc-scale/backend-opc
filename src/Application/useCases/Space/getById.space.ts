import { SpaceDTO } from '@application/dto/Space.dto';
import { SpaceMapper } from '@application/mappers/space.mapper';
import { SpaceRepository } from '@domain/repositories/Spaces';

export class GetSpaceByIdUseCase {
  constructor (
    private readonly spaceRepository: SpaceRepository
  ) {}

  async execute (id: string): Promise<SpaceDTO | undefined> {
    const space = await this.spaceRepository.findById(id);
    if (space === undefined || space === null) return undefined;
    const spaceDto = SpaceMapper.toDTO(space);
    return spaceDto;
  }
}
