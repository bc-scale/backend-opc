import { EventDto } from '@application/dto/Event.dto';
import { EventsMapper } from '@application/mappers/Events.mapper';
import { EventsRepository } from '@domain/repositories/Events';

export class GetByEventIdUseCase {
  constructor (
    private readonly repository: EventsRepository
  ) { }

  async run (id: string): Promise<EventDto> {
    const event = await this.repository.getEventById(id);
    if (event === undefined || event === null) throw new Error('Event not found');
    const eventDto = EventsMapper.toDTO(event);
    return eventDto;
  }
}
