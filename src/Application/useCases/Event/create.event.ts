import { EventDto } from '@application/dto/Event.dto';
import { EventsMapper } from '@application/mappers/Events.mapper';
import { EventsRepository } from '@domain/repositories/Events';
import { StaffRepository } from '@domain/repositories/Staff';

export class CreateEventUseCase {
  private readonly eventsRepository: EventsRepository;
  private readonly staffRepository: StaffRepository;

  constructor (eventsRepository: EventsRepository, staffRepository: StaffRepository) {
    this.eventsRepository = eventsRepository;
    this.staffRepository = staffRepository;
  }

  async run (data: EventDto): Promise<EventDto> {
    const event = EventsMapper.toEntity(data);
    const staff = await this.staffRepository.get(data.user_id);
    if (staff === undefined || staff === null) throw new Error('Staff not found');
    const eventCreated = await this.eventsRepository.createEvent(event);
    if (eventCreated === undefined || eventCreated === null) throw new Error('Event not created');
    return EventsMapper.toDTO(eventCreated);
  }
}
