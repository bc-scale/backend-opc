import { StaffDto } from '@application/dto/Staff.dto';
import { StaffMapperDto } from '@application/mappers/Staff.mapper';
import { UserNotFound } from '@domain/exceptions/UserNotFound';
import { StaffRepository } from '@domain/repositories/Staff';
import { StaffValidations } from '@domain/services/StaffValidations';

export class EditStaffUseCase {
  constructor (private readonly staffRepository: StaffRepository) {}

  async run (props: StaffDto): Promise<StaffDto> {
    const staff = await this.staffRepository.get(props.dni);
    if (staff === undefined || staff === null) throw new UserNotFound('User not found');
    const staffValidations = new StaffValidations(this.staffRepository);
    await staffValidations.runValidationsToEdit(staff);
    const staffUpdated = await this.staffRepository.edit(staff);
    return StaffMapperDto.domainToDto(staffUpdated);
  }
}
