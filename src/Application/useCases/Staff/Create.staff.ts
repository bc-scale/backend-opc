import { StaffDto } from '@application/dto/Staff.dto';
import { StaffMapperDto } from '@application/mappers/Staff.mapper';
import { Exception } from '@domain/exceptions';
import { StaffRepository } from '@domain/repositories/Staff';
import { StaffValidations } from '@domain/services/StaffValidations';
import { Bcrypt } from '@infrastructure/driven-adapters/jwt/Bcrypt';

export class CreateStaffUseCase {
  constructor (private readonly staffRepository: StaffRepository) {}

  async run (props: StaffDto): Promise<StaffDto> {
    const staff = StaffMapperDto.toDomain(props);
    console.log(staff);
    if (staff === undefined || staff === null) throw new Exception('Error creating user');
    const staffValidations = new StaffValidations(this.staffRepository);
    await staffValidations.runValidationsToCreate(staff);
    staff.password = await new Bcrypt(10).hash(staff.password);
    const staffCreated = await this.staffRepository.save(staff);
    return StaffMapperDto.domainToDto(staffCreated);
  }
}
