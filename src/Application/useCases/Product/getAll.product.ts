import { ProductDTO } from '@application/dto/Products.dto';
import { ProductMapper } from '@application/mappers/Product.mapper';
import { ProductRepository } from '@domain/repositories/Products';

export class GetAllProductsUseCase {
  constructor (
    private readonly productsRepository: ProductRepository

  ) {}

  async execute (): Promise<ProductDTO[]> {
    const products = await this.productsRepository.findAll();
    const productsDTO = ProductMapper.toDTOList(products);
    return productsDTO;
  }
}
