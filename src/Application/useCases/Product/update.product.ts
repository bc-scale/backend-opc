import { ProductDTO } from '@application/dto/Products.dto';
import { ProductMapper } from '@application/mappers/Product.mapper';
import { ProductRepository } from '@domain/repositories/Products';

export class UpdateProductUseCase {
  constructor (
    private readonly productsRepository: ProductRepository
  ) {}

  async execute (product: ProductDTO): Promise<ProductDTO> {
    if (product.id === undefined) throw new Error('Product id is required');
    const productAlreadyExists = await this.productsRepository.findById(product.id);
    if (productAlreadyExists === undefined) throw new Error('Product not found');
    const productToUpdate = ProductMapper.toEntity(product);
    const updatedProduct = await this.productsRepository.update(productToUpdate);
    return ProductMapper.toDTO(updatedProduct);
  }
}
