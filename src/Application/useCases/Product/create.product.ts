import { ProductDTO } from '@application/dto/Products.dto';
import { ProductMapper } from '@application/mappers/Product.mapper';
import { ProductRepository } from '@domain/repositories/Products';

export class CreateProductUseCase {
  constructor (
    private readonly productsRepository: ProductRepository
  ) {}

  async execute (product: ProductDTO): Promise<ProductDTO> {
    const productAlreadyExists = await this.productsRepository.findByName(product.name);
    if (productAlreadyExists !== undefined) throw new Error('Product already exists');
    const productToCreate = ProductMapper.toEntity(product);
    const createdProduct = await this.productsRepository.create(productToCreate);
    return ProductMapper.toDTO(createdProduct);
  }
}
