# Capa de aplicación de la aplicación
Esta capa contiene los casos de uso de la aplicación. Los casos de uso son los que se encargan de la comunicación entre las capas de la aplicación.

## Casos de uso
Los casos de uso son los que se encargan de la comunicación entre las capas de la aplicación.

## Mappers
Los mappers son los que se encargan de la conversión de los datos de la capa de dominio a los datos de la capa de aplicación.

### Caso de uso: Crear usuario (Role: SuperAdmin)
Este caso de uso se encarga de crear un usuario en la aplicación si no existe ya un usuario con el mismo dni o email.

### Caso de uso: Editar perfil
Este caso de uso se encarga de editar el perfil de un usuario en la aplicación si el usuario está autenticado.

### Caso de uso: Login
Este caso de uso se encarga de autenticar un usuario en la aplicación si el usuario existe y la contraseña es correcta.

### Caso de uso: Obtener perfil
Este caso de uso se encarga de obtener el perfil de un usuario en la aplicación si el usuario está autenticado.
