export interface SpaceDTO {
  id?: string
  name: string
  number: number
  capacity: number
  state: string
}
