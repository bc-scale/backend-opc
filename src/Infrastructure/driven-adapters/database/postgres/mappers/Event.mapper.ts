import { Event } from '@domain/entities/events/events';
import { EventDao } from '../dao/Event.dao';

export const EventMapper = {
  toDomain: (dao: EventDao): Event => {
    return {
      id: dao.id,
      date: dao.date,
      name: dao.name,
      description: dao.description,
      staff_id: dao.dni_user
    };
  },
  toDao: (event: Event): EventDao => {
    return {
      id: event.id,
      date: event.date,
      name: event.name,
      description: event.description,
      dni_user: event.staff_id
    };
  },
  toDomainList: (daos: EventDao[]): Event[] => {
    return daos.map((dao) => EventMapper.toDomain(dao));
  },
  toDaoList: (events: Event[]): EventDao[] => {
    return events.map((event) => EventMapper.toDao(event));
  }
};
