import { Product } from '@domain/entities/products/products';
import { ProductRepository } from '@domain/repositories/Products';
import { PostgresConnection } from '../../connection';
import { ProductMapper } from '../mappers/Product,mapper';

export class PostgresProductRepository implements ProductRepository {
  private readonly connection: PostgresConnection;

  constructor () {
    this.connection = PostgresConnection.getInstance();
  }

  async create (data: Product): Promise<Product> {
    const query = 'INSERT INTO Products (name) VALUES ($1) RETURNING *';
    const values = [data.name];
    const result = await this.connection.query(query, values);
    return ProductMapper.toDomain(result.rows[0]);
  }

  async update (data: Product): Promise<Product> {
    const query = 'UPDATE Products SET name = $1 WHERE id = $2 RETURNING *';
    const values = [data.name, data.id];
    const result = await this.connection.query(query, values);
    return ProductMapper.toDomain(result.rows[0]);
  }

  async delete (id: string): Promise<Product> {
    const query = 'DELETE FROM Products WHERE id = $1 RETURNING *';
    const result = await this.connection.query(query, [id]);
    return ProductMapper.toDomain(result.rows[0]);
  }

  async findByName (name: string): Promise<Product | undefined> {
    const query = 'SELECT * FROM Products WHERE name = $1';
    const result = await this.connection.query(query, [name]);
    return ProductMapper.toDomain(result.rows[0]);
  }

  async findById (id: string): Promise<Product | undefined> {
    const query = 'SELECT * FROM Products WHERE id = $1';
    const result = await this.connection.query(query, [id]);
    return ProductMapper.toDomain(result.rows[0]);
  }

  async findAll (): Promise<Product[]> {
    const query = 'SELECT * FROM Products';
    const result = await this.connection.query(query);
    return result.rows.map(ProductMapper.toDomain);
  }
}
