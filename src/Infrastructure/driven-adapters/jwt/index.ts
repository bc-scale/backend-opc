import config from '@infrastructure/config/config';
import jwt from 'jsonwebtoken';

export class JWT {
  private readonly secret: string = config.SECRET_KEY ?? '';
  constructor () {
    if (this.secret === '') {
      throw new Error('Secret key not found');
    }
  }

  async sign (payload: any): Promise<string> {
    return jwt.sign(payload, this.secret);
  }

  async verify (token: string): Promise<any> {
    return jwt.verify(token, this.secret);
  }
}
