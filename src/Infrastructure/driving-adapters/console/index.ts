import { LoggerService } from '../../driven-adapters/logger';

(() => {
  const log: LoggerService = new LoggerService();
  log.debug('Welcome to the OPC console ');
})();
