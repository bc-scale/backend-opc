import { ReservationDto } from '@application/dto/Reservation.dto';
import { CreateReservationUseCase } from '@application/useCases/Reservation/create.reservation';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const createReservationController = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const eventsRepository = new PostgresEventsRepository();
  const creator = new CreateReservationUseCase(reservationRepository, eventsRepository);
  const reservation = req.body as ReservationDto;
  const result = await creator.run(reservation);
  res.status(201).json(result);
};
