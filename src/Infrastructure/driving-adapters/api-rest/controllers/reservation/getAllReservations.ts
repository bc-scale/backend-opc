import { GetAllReservationUseCase } from '@application/useCases/Reservation/getAll.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const getAllReservationsController = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new GetAllReservationUseCase(reservationRepository);
  const result = await reservationUseCase.run();
  res.status(200).json(result);
};
