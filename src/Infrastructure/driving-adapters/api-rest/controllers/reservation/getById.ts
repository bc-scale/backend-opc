import { GetByIdReservationUseCase } from '@application/useCases/Reservation/getById.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const getReservationByIdController = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new GetByIdReservationUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.params.id);
  res.status(200).json(result);
};
