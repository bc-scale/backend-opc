import { EventDto } from '@application/dto/Event.dto';
import { CreateEventUseCase } from '@application/useCases/Event/create.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { NextFunction, Request, Response } from 'express';

export const createEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const event = req.body as EventDto;
    const eventRepository = new PostgresEventsRepository();
    const staffRepository = new PostgresStaffRepository();
    const eventUseCase = new CreateEventUseCase(eventRepository, staffRepository);
    const result = await eventUseCase.run(event);
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
};
