import { CreateEventWithReservationUseCase } from '@application/useCases/Event/createWithReservation.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { NextFunction, Request, Response } from 'express';

export const createWithReservation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const reservationRepository = new PostgresReservationRepository();
    const staffRepository = new PostgresStaffRepository();
    const eventUseCase = new CreateEventWithReservationUseCase(eventRepository, reservationRepository, staffRepository);
    const result = await eventUseCase.run(req.body);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
