import { StaffDto } from '@application/dto/Staff.dto';
import { CreateStaffUseCase } from '@application/useCases/Staff/Create.staff';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { NextFunction, Request, Response } from 'express';

const createStaff = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const staff = req.body as StaffDto;
  const postgresStaffRepository = new PostgresStaffRepository();
  const createStaffUseCase = new CreateStaffUseCase(postgresStaffRepository);
  const staffCreated = await createStaffUseCase.run(staff);
  if (staffCreated !== null) {
    res.status(201).json(staffCreated);
  } else {
    res.status(500).json({ message: 'Error creating staff' });
  }
};

export const createUserController = (req: Request, res: Response, next: NextFunction): void => {
  createStaff(req, res, next).catch(next);
};
