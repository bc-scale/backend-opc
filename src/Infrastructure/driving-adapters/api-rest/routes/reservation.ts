import { Router } from 'express';
import { ReservationControllers } from '../controllers/reservation';

const reservationRouter = Router();
const reservationControllers = new ReservationControllers();

reservationRouter.get('/', (req, res, next) => {
  const eventId = req.query.event_id;
  const spaceId = req.query.space_id;
  const userId = req.query.user_id;
  const state = req.query.state;
  if (eventId !== undefined) {
    console.log('get by event');
    void reservationControllers.getByEvent(req, res, next);
  } else if (spaceId !== undefined) {
    res.json('get all reservations by space id');
  } else if (state !== undefined) {
    // res.json('get all reservations by state');
    void reservationControllers.getByState(req, res, next);
  } else if (userId !== undefined) {
    reservationControllers.getByUser(req, res, next);
  } else {
    // res.json('get all reservations');
    void reservationControllers.getAll(req, res, next);
  }
});

reservationRouter.get('/:id', (req, res, next) => {
  // res.json('get reservation by id');
  void reservationControllers.get(req, res, next);
});

reservationRouter.post('/', (req, res, next) => {
  // res.json('create reservation');
  const addProduct = req.query.addProduct;
  if (addProduct !== undefined) {
    // res.json('create reservation with product');
    void reservationControllers.addProduct(req, res, next);
  } else {
    void reservationControllers.create(req, res, next);
  }
});

reservationRouter.put('/:id', (req, res, next) => {
  const state = req.query.state;
  if (state !== undefined) {
    // res.json('update reservation state');
    void reservationControllers.setState(req, res, next);
  } else {
    // res.json('update reservation');
    void reservationControllers.update(req, res, next);
  }
});

reservationRouter.delete('/:id', (req, res, next) => {
  // res.json('delete reservation');
  void reservationControllers.delete(req, res, next);
});

export default reservationRouter;
