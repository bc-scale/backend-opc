/* eslint-disable @typescript-eslint/no-misused-promises */
import { Exception } from '@domain/exceptions';
import { Request, Response, Router, NextFunction } from 'express';
import { login } from '../controllers/login';
import { verifyTokenController } from '../middlewares/verifyToken';
import eventRouter from './event';
import productRouter from './product';
import reservationRouter from './reservation';
import spaceRouter from './space';
import routerStaff from './staff';

const route = Router();

route.get('/ping', function (req, res) {
  res.status(200).json({ message: 'pong' });
});

route.post('/login', login);

route.use('/staff', verifyTokenController, routerStaff);
route.use('/reservation', verifyTokenController, reservationRouter);
route.use('/event', verifyTokenController, eventRouter);
route.use('/product', verifyTokenController, productRouter);
route.use('/space', verifyTokenController, spaceRouter);

route.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof Exception) {
    res.status(400).json({
      message: err.message
    });
  } else {
    next(err);
  }
});

route.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.log(err);
  res.status(500).json({
    error: err
  });
});

export default Router().use('/api/v1', route);
