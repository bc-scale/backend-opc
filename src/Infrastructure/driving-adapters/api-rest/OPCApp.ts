import config from '@infrastructure/config/config';
import { LoggerService } from '@infrastructure/driven-adapters/logger';
import { Server } from './Server';

export class OPCApp {
  server?: Server;
  port?: string;

  async start (): Promise<void> {
    this.port = config.PORT ?? '2426';
    const logger = new LoggerService();
    logger.log('\nenvironment variables:', config);
    this.server = new Server(this.port, logger);
    return await this.server.listen();
  }

  async stop (): Promise<void> {
    return await this.server?.stop();
  }
}
