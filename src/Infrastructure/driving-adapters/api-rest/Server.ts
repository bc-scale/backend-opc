import { ILogger } from '@domain/interfaces/logger';
import express from 'express';
import * as http from 'http';
import morgan from 'morgan';
import routes from './routes';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';

export class Server {
  private readonly _port: string;
  private readonly _app: express.Express;
  private _httpServer?: http.Server;
  logger: ILogger;

  constructor (port: string, logger: ILogger) {
    this._port = port;
    this._app = express();
    this.logger = logger;
    this._app.use(express.json());
    this._app.use(cors(
      {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Authorization']
      }
    ));
    this._app.use(morgan('tiny'));
    this._app.use(routes);
    this._app.use(
      '/docs',
      swaggerUi.serve,
      swaggerUi.setup(undefined, {
        swaggerOptions: {
          url: '/swagger.json'
        }
      })
    );
  }

  async listen (): Promise<void> {
    return await new Promise(resolve => {
      this._httpServer = this._app.listen(this._port, () => {
        this.logger.debug(
          `Mock OPC App is running at http://localhost:${this._port}`
        );
        this.logger.debug('Press CTRL-C to stop\n');
        resolve();
      });
    });
  }

  async stop (): Promise<void> {
    return await new Promise((resolve, reject) => {
      if (this._httpServer !== null && this._httpServer !== undefined) {
        this._httpServer.close(error => {
          if (error !== null && error !== undefined) {
            return reject(error);
          }
          return resolve();
        });
      }

      return resolve();
    });
  }
}
