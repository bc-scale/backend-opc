CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
SELECT uuid_generate_v1();
SELECT uuid_generate_v4();

CREATE TABLE public."Users"
(
    dni text NOT NULL,
    name text NOT NULL,
    cellphone text,
    email text NOT NULL,
    role text NOT NULL,
    password text NOT NULL,
    PRIMARY KEY (dni)
);

ALTER TABLE IF EXISTS public."Users"
    OWNER to postgres;

CREATE TABLE public."Products"
(
    id uuid NOT NULL DEFAULT uuid_generate_v4 (),
    name text,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public."Products"
    OWNER to postgres;

CREATE TABLE public.spaces
(
    id uuid NOT NULL DEFAULT uuid_generate_v4 (),
    name text NOT NULL,
    space_number text NOT NULL,
    capacity integer,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.spaces
    OWNER to postgres;

ALTER TABLE IF EXISTS public.spaces
    RENAME TO "Spaces";

CREATE TABLE public."Events" (
    id uuid NOT NULL DEFAULT uuid_generate_v4 (),
    dni_user text NOT NULL,
    name text NOT NULL,
    date date NOT NULL
);

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT "Events_pkey" PRIMARY KEY (id);

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT fk_users FOREIGN KEY (dni_user) REFERENCES public."Users"(dni);

ALTER TABLE public."Events" OWNER TO postgres;

CREATE TABLE public."Reservations"
(
    created_at date NOT NULL,
    id_event uuid NOT NULL,
    id_space uuid NOT NULL,
    cost_center text NOT NULL,
    overtime boolean NOT NULL DEFAULT False,
    overtime_description text,
    dependence text NOT NULL,
    state text NOT NULL,
    PRIMARY KEY (created_at),
    CONSTRAINT fk_event FOREIGN KEY (id_event)
        REFERENCES public."Events" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_space FOREIGN KEY (id_space)
        REFERENCES public."Spaces" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE IF EXISTS public."Reservations"
    OWNER to postgres;

CREATE TABLE public."ReservationsXProducts"
(
    id uuid NOT NULL DEFAULT uuid_generate_v4 (),
    id_product uuid NOT NULL,
    id_reserve date NOT NULL,
    PRIMARY KEY (id_product),
    CONSTRAINT fk_reservations FOREIGN KEY (id_reserve)
        REFERENCES public."Reservations" (created_at) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_products FOREIGN KEY (id_product)
        REFERENCES public."Products" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE IF EXISTS public."ReservationsXProducts"
    OWNER to postgres;

ALTER TABLE IF EXISTS public."Events"
    ADD COLUMN description text NOT NULL;

INSERT INTO public."Users"(
	dni, name, cellphone, email, role, password)
	VALUES ('1001243609', 'Brandon Velásquez', '3244483045', 'bvelasquez609@soyudemedellin.edu.co', 'SUPERADMIN', '$2a$10$ySXS6BFJii/O.3USG13l5eK4cZAAT4YxfHsA89zrmlBdJPT3jATte') , ('1002345689', 'Sebastian Guzmán', '3243404556', 'sguzman689@soyudemedellin.edu.co', 'SUPERADMIN', '$2a$10$QVo/ZWVAb6Xt7Aa1oTfCqOK.efIhHvUEXGWGs6MnRd0JpAKQZzfw6')
	, ('1002345888', 'Juandy', '3243404556', 'juandy888@soyudemedellin.edu.co', 'SUPERADMIN', '$2a$10$r3O96zQtt9alR0ngZbzRTu9o2oJd5MnHMs77pJX1mnZ3BycJmMSXa') , ('8899008877', 'Santiago Cano', '3243404333', 'sapocano877@soyudemedellin.edu.co', 'opc-assistant', '$2a$10$r3O96zQtt9alR0ngZbzRTu9o2oJd5MnHMs77pJX1mnZ3BycJmMSXa') , ('333444556', 'Dianella Restrepo', '3243444333', 'diane@soyudemedellin.edu.co', 'SUPERADMIN', '$2a$10$Eg1//eVijcXFrEaOYgeoKOKrOSEuwuy83XW6FIj5l.o0Ccr3aC0gi') ;

INSERT INTO public."Spaces"( id,
	name, space_number, capacity)
	VALUES ('eb6d7dca-3053-4fd5-b7ad-a76d4b6d72c4', 'Coliseo', '32', 1234), ('fb7ea51a-5170-44e2-840d-182e318042fe', 'Auditorio 1', '322', 123124), ('09a5b52a-8be9-4df2-9c74-a17d041606da', 'Auditorio 2', '98', 14343), ('e084626d-16de-434b-966a-a65184a0fb12', 'Prometeo', '1', 4332), ('8fa303ff-7406-45d3-a3b9-0e5bc1ae61f4', 'Cancha grama', '08', 5544);

INSERT INTO public."Products"(id, name)
	VALUES ('e111b353-c37d-4f6c-8317-7ff14d05da12', 'Camara 1'), ('e8e427d5-45fd-49cf-8530-0ee47643587d', 'Camara 2'), ( '8b3bd3a5-f530-49ea-a230-834c0a0367c5', 'Camara 3'), ('7496360d-a421-40f5-a259-edc3b0e9d5cf', 'Microfono 1'), ('40ff7c51-1a23-4028-8d97-f7146cd5f2e0', 'Microfono 2');

INSERT INTO public."Events"(id, dni_user, name, date, description) VALUES ('e111b353-c37d-4f6c-8317-7ff14d05da12', '1001243609', 'Evento 1', '2020-10-10', 'Evento de prueba 1'), ('e8e427d5-45fd-49cf-8530-0ee47643587d', '1001243609', 'Evento 2', '2020-10-10', 'Evento de prueba 2'), ('8b3bd3a5-f530-49ea-a230-834c0a0367c5', '1001243609', 'Evento 3', '2020-10-10', 'Evento de prueba 3'), ('7496360d-a421-40f5-a259-edc3b0e9d5cf', '1001243609', 'Evento 4', '2020-10-10', 'Evento de prueba 4'), ('40ff7c51-1a23-4028-8d97-f7146cd5f2e0', '1001243609', 'Evento 5', '2020-10-10', 'Evento de prueba 5');

INSERT INTO public."Reservations"(
	created_at, id_event, id_space, cost_center, overtime, dependence, state)
	VALUES ('2020-10-10', 'e111b353-c37d-4f6c-8317-7ff14d05da12', 'eb6d7dca-3053-4fd5-b7ad-a76d4b6d72c4', 'Centro de costo 1', False, 'Ingenierias', 'PENDING'),
    ('2020-10-02', 'e8e427d5-45fd-49cf-8530-0ee47643587d', 'fb7ea51a-5170-44e2-840d-182e318042fe', 'Centro de costo 2', False, 'Ingenierias', 'PENDING'),
    ('2020-10-11', '8b3bd3a5-f530-49ea-a230-834c0a0367c5', '09a5b52a-8be9-4df2-9c74-a17d041606da', 'Centro de costo 3', False, 'Ingenierias', 'PENDING'),
    ('2020-10-12', '7496360d-a421-40f5-a259-edc3b0e9d5cf', 'e084626d-16de-434b-966a-a65184a0fb12', 'Centro de costo 4', False, 'Ingenierias', 'PENDING'),
    ('2020-10-13', '40ff7c51-1a23-4028-8d97-f7146cd5f2e0', '8fa303ff-7406-45d3-a3b9-0e5bc1ae61f4', 'Centro de costo 5', False, 'Ingenierias', 'PENDING');

INSERT INTO public."ReservationsXProducts"(
	id, id_product, id_reserve)
	VALUES ('e111b353-c37d-4f6c-8317-7ff14d05da12', 'e111b353-c37d-4f6c-8317-7ff14d05da12', '2020-10-10'),
    ('e8e427d5-45fd-49cf-8530-0ee47643587d', 'e8e427d5-45fd-49cf-8530-0ee47643587d', '2020-10-02'),
    ('8b3bd3a5-f530-49ea-a230-834c0a0367c5', '8b3bd3a5-f530-49ea-a230-834c0a0367c5', '2020-10-11'),
    ('7496360d-a421-40f5-a259-edc3b0e9d5cf', '7496360d-a421-40f5-a259-edc3b0e9d5cf', '2020-10-12'),
    ('40ff7c51-1a23-4028-8d97-f7146cd5f2e0', '40ff7c51-1a23-4028-8d97-f7146cd5f2e0', '2020-10-13');